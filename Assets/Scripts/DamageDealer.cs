﻿using UnityEngine;
using System.Collections;

public class DamageDealer : MonoBehaviour {

    public int Damage;
    public GameObject Source;
    public bool Delayed = false;

    private GameObject _delayedReceiver;
    
    void OnCollisionEnter2D(Collision2D coll)
    {
        // disable self damaging
        if (this.Source != null && coll.gameObject.tag == this.Source.tag) return;
        var health = coll.gameObject.GetComponent<Health>();

        if (health != null)
        {
            if (!Delayed)
                health.TakeDamage(Damage);
            else
                _delayedReceiver = coll.gameObject;
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (_delayedReceiver != null && coll.gameObject == _delayedReceiver)
            _delayedReceiver = null;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(string.Format("DD: {0} triggered with {1}", gameObject.name, other.gameObject.name));

        // disable self damaging
        if (this.Source != null && other.gameObject.tag == this.Source.tag) return;
        var health = other.gameObject.GetComponent<Health>();

        if (health != null)
        {
            if (!Delayed)
                health.TakeDamage(Damage);
            else
                _delayedReceiver = other.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (_delayedReceiver != null && other.gameObject == _delayedReceiver)
            _delayedReceiver = null;
    }

    public void DealDamage()
    {
        if(_delayedReceiver != null)
            _delayedReceiver.GetComponent<Health>().TakeDamage(Damage);
    }
}
