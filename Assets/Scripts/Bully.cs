﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Bully : MonoBehaviour
{
    public GameObject PieceOfPaper;
    public AudioClip BlowClip;
    public AudioClip RunAwayClip;
    public AudioClip LaughterClip;

    private const float Speed = 0.2f;
    private const float NearRadius = 2f;

    private GameObject _player;
    private BullyState _currentState;

    private List<GameObject> _spawners = new List<GameObject>();
    private List<GameObject> _targets = new List<GameObject>();

    private Vector3 _positionTarget;

	void Start () {
        _player = GameObject.FindGameObjectWithTag("Player");
        //_spawners = GameObject.FindGameObjectsWithTag("Spawner");
        //_targets = GameObject.FindGameObjectsWithTag("Target");

	    var waypoints = GameObject.Find("Waypoints");
	    foreach (Transform o in waypoints.transform)
	    {
	        if(o.gameObject.tag == "Spawner")
                _spawners.Add(o.gameObject);
            if (o.gameObject.tag == "Target")
                _targets.Add(o.gameObject);
	    }


        _currentState = BullyState.RunningIn;
        Spawn();
	}

    enum BullyState
    {
        RunningIn,
        Blowing,
        RunningOut
    }

	void Update ()
	{
	    
	    if (_currentState == BullyState.RunningIn || _currentState == BullyState.RunningOut)
	    {
            var d = new Vector3(_positionTarget.x - transform.position.x, _positionTarget.y - transform.position.y, 0);
            
            // If reached target
            if (d.magnitude < NearRadius)
            {
                Debug.Log("REACHED!");
                switch (_currentState)
                {
                    case BullyState.RunningIn:
                        var dp = new Vector3(_player.transform.position.x - transform.position.x, _player.transform.position.y - transform.position.y, 0);
                        GetComponent<Animator>().SetFloat("vspeed", -dp.y);
                        GetComponent<Animator>().SetFloat("hspeed", dp.x);
                        GetComponent<Animator>().SetTrigger("BlowTrigger");
                        _currentState = BullyState.Blowing;
                        break;
                    case BullyState.RunningOut:
                        _currentState = BullyState.RunningIn;
                        Spawn();
                        break;
                }
            }
            else
            {
                GetComponent<Animator>().SetFloat("vspeed", -d.y);
                GetComponent<Animator>().SetFloat("hspeed", d.x);
                d.Normalize();
                transform.Translate(d.x * Speed, d.y * Speed, 0);
            }
	    }
	}

    void OnBlow()
    {
        var d = _player.transform.position - transform.position;
        d.Normalize();
        var angle = Mathf.Atan2(d.y, d.x);
        var projectile = ((GameObject)Instantiate(PieceOfPaper, transform.position + 1.3f * new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)), new Quaternion()));
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle));
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;
        AudioSource.PlayClipAtPoint(BlowClip, Vector3.zero);
    }

    void OnBlowEnd()
    {
        _currentState = BullyState.RunningOut;
        AudioSource.PlayClipAtPoint(LaughterClip, Vector3.zero);
        _positionTarget = _spawners[Random.Range(0, _spawners.Count)].transform.position;

    }

    void OnRunAway()
    {

        AudioSource.PlayClipAtPoint(RunAwayClip, Vector3.zero);
    }

    void OnRunIn()
    {

    }

    private void Spawn()
    {
        // Place at the spawner
        var spawnPos = _spawners[Random.Range(0, _spawners.Count)].transform.position;
        transform.position = new Vector3(spawnPos.x, spawnPos.y, 0);
        _positionTarget = _targets[Random.Range(0, _targets.Count)].transform.position;
    }
}
