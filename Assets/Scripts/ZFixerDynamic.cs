﻿using UnityEngine;
using System.Collections;

public class ZFixerDynamic : MonoBehaviour
{
	void Update ()
	{
        const float scaley = 0.5f;
        transform.position = new Vector3(transform.position.x, transform.position.y, (gameObject.collider2D.bounds.center.y) * scaley);
	}
}
