﻿using UnityEngine;
using System.Collections;

public class Cuddles : MonoBehaviour
{
    public GameObject Projectile;
    public GameObject Pawn;
    public AudioClip ThrowClip;
    public AudioClip PunchClip;
	
	void Start () {
        Decide();
	}

    void OnEnable()
    {
        Decide();
    }

    // Update is called once per frame
	void Update () {
	}

    void OnAttack1()
    {
        var playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        AudioSource.PlayClipAtPoint(PunchClip, Vector3.zero);
        Instantiate(Pawn, playerPos, new Quaternion());

        Decide();
    }

    void OnThrow()
    {
        var d = GameObject.FindGameObjectWithTag("Player").transform.position - transform.position;
        d.Normalize();

        var angle = Mathf.Atan2(d.y, d.x);
        var angle2 = angle - Mathf.Deg2Rad * 10;
        var angle3 = angle + Mathf.Deg2Rad * 10;

        AudioSource.PlayClipAtPoint(ThrowClip, Vector3.zero);

        var projectile = ((GameObject)Instantiate(Projectile, transform.position + 1.3f * new Vector3(Mathf.Cos(angle),Mathf.Sin(angle)), new Quaternion()));
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(angle),Mathf.Sin(angle));
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;

        projectile = ((GameObject)Instantiate(Projectile, transform.position + 1.3f * new Vector3(Mathf.Cos(angle2),Mathf.Sin(angle2)), new Quaternion()));
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(angle2),Mathf.Sin(angle2));
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;

        projectile = ((GameObject)Instantiate(Projectile, transform.position + 1.3f * new Vector3(Mathf.Cos(angle3),Mathf.Sin(angle3)), new Quaternion()));
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(angle3),Mathf.Sin(angle3));
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;

        Decide();
    }

    void OnHug()
    {
        //play sound
        this.GetComponent<Animator>().SetTrigger("AttackTrigger");
    }

    private void Decide()
    {
        var rand = Random.Range(0, 1f);
        if (rand < 0.6f)
        {
            GetComponent<Animator>().SetTrigger("ThrowTrigger");
            return;
        }

        if (rand < 0.8f)
        {
            GetComponent<Animator>().SetTrigger("AttackTrigger");
            return;
        }
        
        GetComponent<Animator>().SetTrigger("HugTrigger");
        return;
    }
}
