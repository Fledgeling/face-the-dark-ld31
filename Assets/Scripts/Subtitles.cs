﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Subtitles : MonoBehaviour
{

    private bool _showing = false;
    private string _caption;
    private string _text;
    private float _subtitlesExists;
    public Action OnSubtitlesEnd;

    private const float MinSubtitlesTime = 1f;
	
	// Update is called once per frame
	void Update () {
        if (_showing)
	    {
            if(_subtitlesExists > MinSubtitlesTime && Input.GetButton("Interact"))
	            GetComponent<Animator>().SetTrigger("Hide");
	        _subtitlesExists += Time.deltaTime;
	    }
	}

    void OnShow()
    {
        GameObject.FindGameObjectWithTag("Player").SendMessage("OnStun");
        GameObject.Find("SubtitlesCaption").GetComponent<Text>().text = _caption;
        GameObject.Find("SubtitlesText").GetComponent<Text>().text = _text;
        _showing = true;
    }

    void OnHide()
    {
        GameObject.FindGameObjectWithTag("Player").SendMessage("OnFree");
        GameObject.Find("SubtitlesCaption").GetComponent<Text>().text = "";
        GameObject.Find("SubtitlesText").GetComponent<Text>().text = "";
        GetComponent<Animator>().ResetTrigger("Hide");
        if (OnSubtitlesEnd != null)
            OnSubtitlesEnd();
        _showing = false;
    }

    public void Show(string caption, string text)
    {
        _subtitlesExists = 0;
        _caption = caption;
        _text = text;
        this.GetComponent<Animator>().SetTrigger("Show");
    }
}
