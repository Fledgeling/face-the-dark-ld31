﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DarkOne : MonoBehaviour
{
    public AudioClip DarkRunClip;
    
    private const float Speed = 0.4f;
    private const float MournSpeed = 0.02f;
    private const float NearRadius = 1f;

    private GameObject _player;
    private GameObject _label;
    private DarkOneState _currentState;

    private List<GameObject> _spawners = new List<GameObject>();
    private List<GameObject> _targets = new List<GameObject>();

    private Vector3 _positionTarget;

    void Start()
    {
        _label = GameObject.Find("InteractLabel");
        _player = GameObject.FindGameObjectWithTag("Player");
        //_spawners = GameObject.FindGameObjectsWithTag("Spawner");
        //_targets = GameObject.FindGameObjectsWithTag("Target");

        var waypoints = GameObject.Find("Waypoints");
        foreach (Transform o in waypoints.transform)
        {
            if (o.gameObject.tag == "Spawner")
                _spawners.Add(o.gameObject);
            if (o.gameObject.tag == "Target")
                _targets.Add(o.gameObject);
        }


        _currentState = DarkOneState.RunningIn;
        _player.GetComponent<Health>().OnHealthChanged += OnPlayerHealthChanged;
        GetComponent<Health>().SetImmune(true);
        Spawn();
    }

    private void OnPlayerHealthChanged(Health health, int newval)
    {
        if (newval <= 1)
        {
            audio.Play();
            GetComponent<Animator>().SetTrigger("Mourn");
            _currentState = DarkOneState.Mourn;
            GetComponent<DamageDealer>().Damage = 0;
            
            var h = GetComponent<Health>();
            h.enabled = true;
            h.SetImmune(false);
        }
    }

    enum DarkOneState
    {
        RunningIn,
        RunningOut,
        Mourn
    }

    void Update()
    {
        if (_currentState == DarkOneState.RunningIn || _currentState == DarkOneState.RunningOut)
        {
            var d = new Vector3(_positionTarget.x - transform.position.x, _positionTarget.y - transform.position.y, 0);

            // If reached target
            if (d.magnitude < NearRadius)
            {
                Debug.Log("REACHED!");
                switch (_currentState)
                {
                    case DarkOneState.RunningIn:
                        var dp = new Vector3(_player.transform.position.x - transform.position.x, _player.transform.position.y - transform.position.y, 0);
                        GetComponent<Animator>().SetFloat("vspeed", -dp.y);
                        GetComponent<Animator>().SetFloat("hspeed", dp.x);
                        _currentState = DarkOneState.RunningOut;
                        _positionTarget = _spawners[Random.Range(0, _spawners.Count)].transform.position;
                        AudioSource.PlayClipAtPoint(DarkRunClip, Vector3.zero);
                        break;
                    case DarkOneState.RunningOut:
                        _currentState = DarkOneState.RunningIn;
                        Spawn();
                        break;
                }
            }
            else
            {
                GetComponent<Animator>().SetFloat("vspeed", -d.y);
                GetComponent<Animator>().SetFloat("hspeed", d.x);
                d.Normalize();
                transform.Translate(d.x*Speed, d.y*Speed, 0);
            }
        }
        else if(_currentState == DarkOneState.Mourn)
        {
            var d = _player.transform.position - transform.position;

            if (d.magnitude > NearRadius * 2f)
            {
                d.Normalize();
                transform.Translate(d*MournSpeed);
                _label.guiText.enabled = false;
            }
            else
            {
                if (_label != null & Camera.current != null)
                {
                    _label.guiText.enabled = true;
                    _label.guiText.text = "?";
                    _label.transform.position = Camera.current.WorldToViewportPoint(this.transform.position);
                }
                if (Input.GetAxis("Interact") > 0)
                {
                    GameObject.Find("RoomManager").GetComponent<RoomManager>().Switch("Room11");
                    _player.GetComponent<Health>().Restore();
                }
            }
        }
    }

    private void Spawn()
    {
        // Place at the spawner
        var spawnPos = _spawners[Random.Range(0, _spawners.Count)].transform.position;
        transform.position = new Vector3(spawnPos.x, spawnPos.y, 0);
        //_positionTarget = _targets[Random.Range(0, _targets.Count)].transform.position;
        _positionTarget = _player.transform.position;
    }
}
