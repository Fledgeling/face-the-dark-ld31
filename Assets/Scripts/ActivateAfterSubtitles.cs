﻿using UnityEngine;
using System.Collections;

public class ActivateAfterSubtitles : MonoBehaviour
{

    private Subtitles _subtitles;
    private RoomManager _roomManager;
	
	void Start ()
	{
	    _subtitles = GameObject.Find("Subtitles").GetComponent<Subtitles>();
        _roomManager = GameObject.Find("RoomManager").GetComponent<RoomManager>();
	    _subtitles.OnSubtitlesEnd += OnSubtitlesEnd;
        _roomManager.OnEndSwitch += OnEndSwitch;
        foreach (Transform child in transform)
	    {
	        child.gameObject.SetActive(false);
	    }
	}

    private void OnEndSwitch(string obj)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    void OnSubtitlesEnd()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }
}
