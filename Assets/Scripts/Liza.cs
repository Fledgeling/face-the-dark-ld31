﻿using UnityEngine;
using System.Collections;

public class Liza : MonoBehaviour {

	// Use this for initialization
    public GameObject Projectile;// потом осколки от тарелки вставить можно
    public AudioClip LizaPlateClip;
    public AudioClip LizaScreamClip;
    public AudioClip LizaLaughterClip;

    float signa;
	void Start () {
    
        OnRespawn();
       	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnPlateThrow()
    {
        var angle = Mathf.PI / 4 * 0;
         var projectile = ((GameObject)Instantiate(Projectile, transform.position + 1.3f * new Vector3(Mathf.Cos(0),Mathf.Sin(0)), new Quaternion()));
         AudioSource.PlayClipAtPoint(LizaPlateClip, Vector3.zero);
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(0),Mathf.Sin(0));
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;
        for (int i=1; i<8; i++){
            angle=Mathf.PI/4*i;
        projectile = ((GameObject)Instantiate(Projectile, transform.position + 1.3f * new Vector3(Mathf.Cos(angle),Mathf.Sin(angle)), new Quaternion()));
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(Mathf.PI / 4 * i), Mathf.Sin(Mathf.PI / 4 * i));
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;}
        GetComponent<Animator>().SetTrigger("SpawnTrigger");
        
    }

    void OnScream()
    {

        var angle = Mathf.PI / 4 * 0;
        var projectile = ((GameObject)Instantiate(Projectile, transform.position + 1.3f * new Vector3(Mathf.Cos(0), Mathf.Sin(0)), new Quaternion()));
        AudioSource.PlayClipAtPoint(LizaScreamClip, Vector3.zero);
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(0), Mathf.Sin(0));
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;
        for (int i = 1; i < 8; i++)
        {
            angle = signa * Mathf.PI / 8 * i;
            projectile = ((GameObject)Instantiate(Projectile, transform.position + 1.3f * new Vector3(Mathf.Cos(angle), Mathf.Sin(angle)), new Quaternion()));
            projectile.GetComponent<Projectile>().Speed = 0.2f;
            projectile.GetComponent<Projectile>().Direction = new Vector3(Mathf.Cos(signa * Mathf.PI / 8 * i), Mathf.Sin(signa * Mathf.PI / 8 * i));
            projectile.GetComponent<DamageDealer>().Source = this.gameObject;
        }
        
            Decide1();
            
        
    }

    void OnSpawn()
    {
        AudioSource.PlayClipAtPoint(LizaLaughterClip, Vector3.zero);
        transform.position = new Vector3(Random.Range(-7f, 7f), Random.Range(-3f, 3f),10);
        GetComponent<Animator>().SetTrigger("RespawnTrigger");
    }

    void OnRespawn()
    {
        var d = GameObject.FindGameObjectWithTag("Player").transform.position - transform.position;
        d.Normalize();
        this.GetComponent<Animator>().SetFloat("Looky", d.y);
        signa =-1* Mathf.Sign(d.y);
        this.GetComponent<Animator>().SetFloat("Lookx", d.x);
        GetComponent<Animator>().ResetTrigger("RespawnTrigger");
        GetComponent<Animator>().SetTrigger("ScreamTrigger");
     
    }

    private void Decide1()
    {
        var rand = Random.Range(0, 1f);
        if (rand < 0.8f)
        {
            GetComponent<Animator>().SetTrigger("PlateTrigger");
            return;
        }
        GetComponent<Animator>().SetTrigger("SpawnTrigger");
        return;
    }
    //private void Decide2()
    //{
    //    var rand = Random.Range(0, 1f);
    //    if (rand < 0.5f)
    //    {
    //        GetComponent<Animator>().SetTrigger("ScreamTrigger");
    //        return;
    //    }
    //    GetComponent<Animator>().SetTrigger("SpawnTrigger");
    //    return;
   // }
}
