﻿using UnityEngine;
using System.Collections;

public class Doctor : MonoBehaviour
{
    public float Speed = 0.06f;
    public GameObject NursePrefab;
    public AudioClip DoctorSpawnClip;
    public AudioClip DoctorAttackClip;

    private GameObject _player;
    
    private DoctorState _currentState;
    private int _nursesSpawned = 0;
    private float _nursesSpawnRate = 0;
    private Vector3 _spawnNextNurseAt;

    private const float AttackRange = 1f;
    private const float NursesSpawnRateMax = 3f;
    private const int MaxNurses = 300;

    enum DoctorState
    {
        Follow,
        Attack,
        SpawnNurses
    }
    
	void Start ()
	{
        _player = GameObject.FindGameObjectWithTag("Player");
        _currentState = DoctorState.Follow;
	    _nursesSpawnRate = NursesSpawnRateMax;
	}
	
	void Update ()
	{
	    var d = new Vector3(_player.transform.position.x - transform.position.x,_player.transform.position.y - transform.position.y, 0);
        
	    if (_currentState == DoctorState.Follow)
	    {
	        if (d.magnitude < AttackRange)
	        {
                _currentState = DoctorState.Attack;
                GetComponent<Animator>().SetTrigger("AttackTrigger");
                this.GetComponent<Animator>().SetFloat("vspeed", 0);
                this.GetComponent<Animator>().SetFloat("hspeed", d.x);
	        }
	        else
	        {
	            d.Normalize();
	            this.GetComponent<Animator>().SetFloat("vspeed", -d.y);
	            this.GetComponent<Animator>().SetFloat("hspeed", d.x);
                transform.Translate(d.x * Speed, d.y * Speed, 0);
	            _nursesSpawnRate -= Time.deltaTime;
	            if (_nursesSpawned < MaxNurses && _nursesSpawnRate < 0 && !_player.GetComponent<Player>().IsStunned)
	            {
                    GetComponent<Animator>().SetFloat("vspeed", 0);
                    GetComponent<Animator>().SetFloat("hspeed", 0);
	                _currentState = DoctorState.SpawnNurses;
                    GetComponent<Animator>().SetTrigger("NurseTrigger");
	            }
	        }
	    }
	}

    void OnStartSpawnNurse()
    {
        _spawnNextNurseAt = _player.transform.position;
        AudioSource.PlayClipAtPoint(DoctorSpawnClip, Vector3.zero);
    }

    void OnSpawnNurse()
    {
        _nursesSpawnRate = NursesSpawnRateMax;
        _nursesSpawned++;
        _currentState = DoctorState.Follow;
        Instantiate(NursePrefab, _spawnNextNurseAt, new Quaternion());
    }

    void OnShieldOn()
    {

    }

    void OnShieldOff()
    {

    }

    void OnAttackStart()
    {
        AudioSource.PlayClipAtPoint(DoctorAttackClip, Vector3.zero);
        GetComponentInChildren<DamageDealer>().DealDamage();
    }

    void OnAttackEnd()
    {
        _currentState = DoctorState.Follow;
    }

}
