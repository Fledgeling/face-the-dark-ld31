﻿using UnityEngine;
using System.Collections;

public class ZFixerStatic : MonoBehaviour {

    private void Fix()
    {
        const float scaley = 0.5f;
        transform.position = new Vector3(transform.position.x, transform.position.y, (gameObject.collider2D.bounds.center.y) * scaley);
    }

	// Use this for initialization
	void Start ()
	{
	    GameObject.Find("RoomManager").GetComponent<RoomManager>().OnEndSwitch += (e) => Fix();
        Fix();
	}
}
 