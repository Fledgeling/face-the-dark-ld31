﻿using UnityEngine;
using System.Collections;

public class Interactive : MonoBehaviour
{
    public string InteractionCaption;
    public string SwitchToRoom;
    public bool Locked;

    private GameObject _label;
    private bool _playerNear;
    private bool _interacted;
    private SpriteRenderer _renderer;
    private static readonly Color ActiveColor = new Color(1f,1f,1f,1f);
    private static readonly Color LockedColor = new Color(.5f, .5f, .5f, .8f);

    void Start()
    {
        _playerNear = false;
        _label = GameObject.Find("InteractLabel");
        _interacted = false;
        _renderer = GetComponent<SpriteRenderer>();
        if (_label != null)
            _label.guiText.text = "";
        GameObject.Find("RoomManager").GetComponent<RoomManager>().OnStartSwitch += (e) => _label.guiText.text = "";

        if(_renderer!=null)
            _renderer.color = Locked ? LockedColor : ActiveColor;
    }

    public void OnEnable()
    {
        Start();
    }

    private void Interact()
    {
        Debug.Log("Activated!!!");
        if (!string.IsNullOrEmpty(SwitchToRoom))
            GameObject.Find("RoomManager").SendMessage("Switch", SwitchToRoom);
    }

    private void Update()
    {
        if (_playerNear)
        {
            if (!Locked && Input.GetButton("Interact"))
            {
                if (!_interacted)
                {
                    Interact();
                    _interacted = true;
                    _playerNear = false;
                }
            }
            else
                _interacted = false;


            if (_label != null & Camera.current != null)
                _label.transform.position = Camera.current.WorldToViewportPoint(this.transform.position);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            _playerNear = true;

            if (_label != null)
            {
                _label.SetActive(true);
                _label.GetComponent<GUIText>().text = Locked ? InteractionCaption + " (Locked)" : InteractionCaption;
            }
        }
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            _playerNear = false;

            if (_label != null)
                _label.SetActive(false);
        }
    }

    void LockInteractive()
    {
        Locked = true;
        if (_renderer != null)
            _renderer.color = Locked ? LockedColor : ActiveColor;
    }

    void UnlockInteractive()
    {
        Locked = false;
        if (_renderer != null)
            _renderer.color = Locked ? LockedColor : ActiveColor;
    }
}
