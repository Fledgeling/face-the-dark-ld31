﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class Locker : MonoBehaviour
{

    public GameObject Jaws;
    public GameObject WallPart1;
    public GameObject WallPart2;
    public GameObject WallPart3;
    public AudioClip LockerBreakClip;
    //public AudioClip LockerFlyAwayClip;
   // public AudioClip RoomJawsClip;
    //public AudioClip RoomWallClip;

    private float _t;
    private const float Delay = 2f;
    private RoomState _currentState;
    private List<GameObject> _targets = new List<GameObject>();
    private const int JawsCount = 3;
    private readonly int[] _indexes = new int[JawsCount];

    enum RoomState
    {
        Locker,
        Jaws,
        Walls
    }

	void Start () {
	    _currentState = RoomState.Locker;
	    //_targets = GameObject.FindGameObjectsWithTag("Target");

        var waypoints = GameObject.Find("Targets");
        foreach (Transform o in waypoints.transform)
        {
            if (o.gameObject.tag == "Target")
                _targets.Add(o.gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if (_currentState == RoomState.Locker) return;
	    if (_t > Delay)
	    {
	        if (_currentState == RoomState.Jaws)
	        {
	            SpawnJaws();
	            Decide();
	        }
	        if (_currentState == RoomState.Walls)
	        {
	            SpawnWallParts();
	            Decide();
	        }
	        _t = 0;
	    }
	    _t += Time.deltaTime;
	}

    void OnIn()
    {
        GetComponent<Animator>().SetTrigger("OutTrigger");
        AudioSource.PlayClipAtPoint(LockerBreakClip, Vector3.zero);
    }

    void OnOut()
    {
        Decide();
       // AudioSource.PlayClipAtPoint(LockerFlyAwayClip, Vector3.zero);
    }

    private void Decide()
    {
        var rand = Random.Range(0, 1f);
        if (rand < 0.33f)
        {
            _currentState = RoomState.Walls;
            return;
        }
        if (rand < 0.66f)
        {
            _currentState = RoomState.Jaws;
            return;
        }

        _currentState = RoomState.Locker;
        GetComponent<Animator>().SetTrigger("InTrigger");
        this.transform.position = _targets[Random.Range(0, _targets.Count)].transform.position;
        return;
    }

    private void SpawnJaws()
    {
        //AudioSource.PlayClipAtPoint(RoomJawsClip, Vector3.zero);
        for (var i = 0; i < JawsCount; i++)
            _indexes[i] = -1;
        var curCount = 0;
        while (curCount<3)
        {
            var i = Random.Range(0, _targets.Count);
            if (!_indexes.Contains(i))
            {
                _indexes[curCount] = i;
                curCount++;

                Instantiate(Jaws, _targets[i].transform.position, new Quaternion());
            }
        }
    }

    private GameObject GetRandomWallPart()
    {
        switch (Random.Range(1,3))
        {
            case 1:
                return WallPart1;
            case 2:
                return WallPart2;
            case 3:
                return WallPart3;

        }
        return null;
    }

    private void SpawnWallParts()
    {
        var i = Random.Range(1, 4);
        var o1 = GameObject.Find(i + "-1");
        var o2 = GameObject.Find(i + "-2");

        //AudioSource.PlayClipAtPoint(RoomWallClip, Vector3.zero);

        var projectile = ((GameObject)Instantiate(GetRandomWallPart(), o1.transform.position, new Quaternion()));
        var d = GameObject.FindWithTag("Player").transform.position - o1.transform.position;
        d.Normalize();
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(d.x, d.y);
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;

        projectile = ((GameObject)Instantiate(GetRandomWallPart(), o2.transform.position, new Quaternion()));
        d = GameObject.FindWithTag("Player").transform.position - o2.transform.position;
        d.Normalize();
        projectile.GetComponent<Projectile>().Speed = 0.2f;
        projectile.GetComponent<Projectile>().Direction = new Vector3(d.x, d.y);
        projectile.GetComponent<DamageDealer>().Source = this.gameObject;
    }
}
