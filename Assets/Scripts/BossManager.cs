﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class BossManager : MonoBehaviour
{

    private Dictionary<string, GameObject> _bosses = new Dictionary<string, GameObject>();
    private RoomManager _rm;
    private string _activeBoss = "";
	
	void Start () {
        foreach (var o in GameObject.FindGameObjectsWithTag("Boss"))
	    {
	        _bosses.Add(o.name,o);
            Debug.Log(o.name);
            o.SetActive(false);
	    }
        _rm = GameObject.Find("RoomManager").GetComponent<RoomManager>();
	    _rm.OnStartSwitch += OnRoomStartSwitch;
        GameObject.Find("Subtitles").GetComponent<Subtitles>().OnSubtitlesEnd += OnSubtitlesEnd;
	    GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().OnHealthChanged += OnPlayerHealthChanges;
	}

    private void OnRoomStartSwitch(string obj)
    {
        foreach (var o in GameObject.FindGameObjectsWithTag("Boss"))
        {
            o.SetActive(false);
            Debug.Log(o.name);
        }
    }

    private void OnPlayerHealthChanges(Health hp, int val)
    {
        if (val <= 0)
        {
            hp.Restore();
            hp.HideHealthBar();
            GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().Restore();
            _rm.Switch("Room1");
        }
    }

    private void OnSubtitlesEnd()
    {
        foreach (Transform o in _rm.CurrentRoomObject.transform)
        {
            if (o.gameObject.tag == "Boss")
            {
                o.gameObject.SetActive(true);
                var bossHealth = o.GetComponentInChildren<Health>();
                if (bossHealth != null)
                {
                    bossHealth.OnHealthChanged += OnBossHealtChanged;
                    bossHealth.InitHealthBar();
                }
                return;
            }
        }
    }

    private void OnBossHealtChanged(Health health, int val)
    {
        // if boss defeated
        if (health.CurrentHealth <= 0)
        {
            if (!health.gameObject.name.Contains("Dark"))
            {
                _rm.Switch("Room1");
                health.HideHealthBar();
                var h = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
                h.Restore();
                ItemsCheck(health.gameObject.name);
            }
            else
                _rm.Switch("Room10");//BAD ENDING
        }
    }

    private List<string> _defeatedBosses = new List<string>();
    private void ItemsCheck(string lastDefeated)
    {
        var itemToChange = "";
        switch (lastDefeated)
        {
            case "Bully": itemToChange = "ItemDiary"; break;
            case "Liza": itemToChange = "ItemRing"; break;
            case "Locker": itemToChange = "ItemCage"; break;
            case "LockerClosed": itemToChange = "ItemCage"; break;
            case "Cuddles": itemToChange = "ItemToy"; break;
            case "Doctor": itemToChange = "ItemPills"; break;
        }
        if(!_defeatedBosses.Contains(lastDefeated))
            _defeatedBosses.Add(lastDefeated);

        if (itemToChange != "")
        {
            Debug.Log("Trying to lock item: " + itemToChange);
            GameObject.Find(itemToChange).SendMessage("LockInteractive");
        }
        if(_defeatedBosses.Count >= 5)
            GameObject.Find("ItemNote").SendMessage("UnlockInteractive");
    }
}
