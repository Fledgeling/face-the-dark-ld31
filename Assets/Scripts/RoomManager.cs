﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoomManager : MonoBehaviour
{
    struct RoomObjectDescr
    {
        public Transform Transform;
        public Vector3 StartPos;
        public Vector3 EndPos;
    }

    public string CurrentRoom { get { return _oldRoom.name; } }
    public GameObject CurrentRoomObject { get { return _oldRoom; } }

    public event Action<string> OnStartSwitch;
    public event Action<string> OnEndSwitch;

    private bool _switching;
    private float _t;
    private Vector3 _newRoomOffset;
    //private Vector3 _oldRoomOffset;
    private GameObject _oldRoom;
    private GameObject _newRoom;
    private Subtitles _subtitles;
    private readonly List<RoomObjectDescr> _newRoomChilds = new List<RoomObjectDescr>();
    private readonly List<RoomObjectDescr> _oldRoomChilds = new List<RoomObjectDescr>();
    private const float SwitchTime = 2f;
    
    // Координаты, откуда выезжает комната
    private static readonly Vector3 _fromPos = new Vector3(0, -30, 0);

    // Координаты куда приезжает комната
    private static readonly Vector3 _toPos = new Vector3(0, -30, 0);

    private Dictionary<string, GameObject> _rooms = new Dictionary<string, GameObject>();

    void Start ()
	{
	    _switching = false;
        foreach (var o in GameObject.FindGameObjectsWithTag("Room"))
            _rooms.Add(o.name,o);
        _oldRoom = GameObject.Find("Room1");
        //foreach (var room in _rooms.Values)
           // if (room.name != "Room1") 
                //room.SetActive(false);
        _subtitles = GameObject.Find("Subtitles").GetComponent<Subtitles>();
        _subtitles.Show("Beginning","Suicide notes are usually written for someone, but this is not the case. I’m not sure who will read it if anyone would actually find me in this apartment, so you could say I’m writing it for myself. I can’t handle the situation anymore, I keep questioning the reality I see thanks to my… condition, even know. My memories are shattered and there is no one to care about me, I suppose. It’s a fair decision, isn’t it?");
	}

	void Update () 
    {
	    if (_switching)
	    {
            _t += Time.deltaTime;
            if (_t > SwitchTime)
                _t = SwitchTime;

	        foreach (var o in _oldRoomChilds)
                    o.Transform.position = Vector3.Slerp(o.StartPos, o.EndPos, _t / SwitchTime);

	        foreach (var o in _newRoomChilds)
                    o.Transform.position = Vector3.Slerp(o.StartPos, o.EndPos, _t / SwitchTime);

            if (_t >= SwitchTime)
	            OnSwitchEnd();
	    }
	}

    private void OnSwitchEnd()
    {
        _switching = false;
        var player = GameObject.FindGameObjectWithTag("Player");
        player.collider2D.enabled = true;
        player.SendMessage("OnFree");
        _oldRoom.SetActive(false);
        _oldRoom = _newRoom;
        _newRoom = null;

        /*
        foreach (var o in _newRoomChilds)
            o.Transform.position = o.EndPos;
        foreach (var o in _oldRoomChilds)
            o.Transform.position = o.EndPos;
        */

        if (OnEndSwitch != null)
            OnEndSwitch(_oldRoom.name);
    }

    public void Switch(string roomName)
    {
        var oldRoom = _oldRoom.name;
        if (OnStartSwitch != null)
            OnStartSwitch(_oldRoom.name);
        var player = GameObject.FindGameObjectWithTag("Player");
        player.collider2D.enabled = false;
        player.SendMessage("OnStun");

        _t = 0f;
        _newRoom = _rooms[roomName];
        _newRoom.SetActive(true);

        //_oldRoomOffset = _oldRoom.transform.position;
        _newRoomOffset = _newRoom.transform.position;

        _oldRoomChilds.Clear();
        foreach (var o in _oldRoom.GetComponentsInChildren<Transform>())
        {
            var startPos = o.transform.position;
            var endPos = o.transform.position + _toPos;
            _oldRoomChilds.Add(new RoomObjectDescr()
            {
                StartPos = startPos,
                Transform = o.transform,
                EndPos = endPos,
                //Delay = Random.Range(0, MaxDelay / 2)
            });
        }

        _newRoomChilds.Clear();
        foreach (var o in _newRoom.GetComponentsInChildren<Transform>())
        {
            var startPos = o.transform.position - _newRoomOffset + _fromPos;
            //startPos += new Vector3(0,UnityEngine.Random.Range(0,15),0);
            startPos += new Vector3(0,o.transform.localPosition.y * 2f,0);
            var endPos = o.transform.position - _newRoomOffset;
            
            _newRoomChilds.Add(new RoomObjectDescr()
            {
                StartPos = startPos,
                EndPos = endPos,
                Transform = o.transform,
                //Delay = Random.Range(MaxDelay / 2, MaxDelay)
            });
        }
        _switching = true;

        switch (roomName)
        {
            case "Room1":
                switch (oldRoom)
                {
                    case "Room3":
                        _subtitles.Show("", "First day is supposed to be the happiest, the brightest one. In my case, it was just an ordinary day, as good as schooldays that were yet to come. And they were not good at all."); 
                        break;
                    case "Room6":
                        _subtitles.Show("", "If you still think I grew up a strong individual with some self-esteem, you didn’t pay attention. Bullying in school hadn’t stopped all of a sudden, developing more fears to face");
                        break;
                    case "Room5":
                        _subtitles.Show("", "I was considered just a harmless weirdo back in school, but my doctor told me that my condition is bad and probably started years ago. Felt ambivalence seemed nothing more than a lack of confidence for me, but then everything moved to just a completely new level of shit, when I stopped recognizing my wife, startling at night thinking that our little girl is screaming. However, our only child is a son. Was a son.");
                        break;
                    case "Room4":
                        _subtitles.Show("", "Lisa. The one who made me a bit less socially awkward and a bit happier with my life. She gave me everything, but then she left and took away even more. I know I can’t blame her for it, I was… I am sick. But I really, really hate her for leaving me.");
                        break;
                    case "Room2":
                        _subtitles.Show("", "My life has gone to shit earlier but this was it. When John died, I’ve completely lost myself. There was nothing for me to keep living, nothing to help me bonding with reality anymore. I was depressed, but still a coward; it took me 2 years to make one final step into the darkness.");        
                        break;
                }
                break;
            case "Room3":
                //_subtitles.Show("1984, First day in school", "First day is supposed to be the happiest, the brightest one. In my case, it was just an ordinary day, as good as schooldays that were yet to come. And they were not good at all.");
                _subtitles.Show("", "1984, First day in school");
                break;
            case "Room6":
                //_subtitles.Show("1988, First case of claustrophobia", "If you still think I grew up a strong individual with some self-esteem, you didn’t pay attention. Bullying in school hadn’t stopped all of a sudden, developing more fears to face");
                _subtitles.Show("", "1988, First case of claustrophobia");
                break;
            case "Room5":
                //_subtitles.Show("2005, Schizophrenia diagnosed", "I was considered just a harmless weirdo back in school, but my doctor told me that my condition is bad and probably started years ago. Felt ambivalence seemed nothing more than a lack of confidence for me, but then everything moved to just a completely new level of shit, when I stopped recognizing my wife, startling at night thinking that our little girl is screaming. However, our only child is a son. Was a son.");
                _subtitles.Show("", "2005, Schizophrenia diagnosed");
                break;
            case "Room4":
                //_subtitles.Show("2006, the day Lisa left me", "Lisa. The one who made me a bit less socially awkward and a bit happier with my life. She gave me everything, but then she left and took away even more. I know I can’t blame her for it, I was… I am sick. But I really, really hate her for leaving me.");
                _subtitles.Show("", "2006, the day Lisa left me");
                break;
            case "Room2":
                //_subtitles.Show("2008, the day Johnny died", "My life has gone to shit earlier but this was it. When John died, I’ve completely lost myself. There was nothing for me to keep living, nothing to help me bonding with reality anymore. I was depressed, but still a coward; it took me 2 years to make one final step into the darkness.");
                _subtitles.Show("", "2008, the day Johnny died");
                break;
            case "Room7":
                _subtitles.Show("", "Present day");
                break;
            case "Room11": // GOOD
                _subtitles.Show("", "I’ve faced the darkness, and it has not frightened me. I am my own enemy, and pretending that all the bad things that happened to me somehow justifies the suicide doesn’t help. I can fight this illness, not by gun, but by accepting the problem and asking for a help.");
                break;
            case "Room10": // BAD
                _subtitles.Show("", "I saw the darkness and it frightens me, but I am afraid of myself more. I can’t fight with my illness with a gun, as much as I can’t pretend that everything’s alright. I can’t leave my former goal incomplete, can I?");
                break;
        }
    }
}
