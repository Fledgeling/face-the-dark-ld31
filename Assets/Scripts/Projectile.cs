﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float Speed;
    public Vector2 Direction;
    public float TTL = 1f;
    

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frames
    void Update()
    {
        this.transform.Translate(Direction.x * Speed, Direction.y * Speed, 0);
        TTL -= Time.deltaTime;
        if (TTL < 0)
            Destroy(this.gameObject);
        
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        Destroy(this.gameObject);
        Debug.Log("Projectile collision with " + coll.gameObject.name);
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        Destroy(this.gameObject);
        Debug.Log("Projectile collision with " + coll.gameObject.name);
    }
}