﻿using UnityEngine;
using System.Collections;

public class Nurse : MonoBehaviour {

    private GameObject _player;
    public AudioClip NurseSpawnClip;
    public AudioClip NurseFallClip;

    private const float GrabRange = 2f;
    private float _ttl = 5f;
    private bool _grabbing = false;
        
	void Start () 
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        AudioSource.PlayClipAtPoint(NurseSpawnClip, Vector3.zero);
	}
	
	// Update is called once per frame
	void Update () 
    {
        var d = new Vector3(_player.transform.position.x - transform.position.x, _player.transform.position.y - transform.position.y, 0);
	    if (!_grabbing)
	    {
	        this.GetComponent<Animator>().SetFloat("hspeed", d.x);
	        this.GetComponent<Animator>().SetFloat("vspeed", -d.y);
	    }

	    _ttl -= Time.deltaTime;

	    if (_ttl < 0)
	    {
            GetComponent<Animator>().SetFloat("vspeed", 0);
            GetComponent<Animator>().SetFloat("hspeed", 0);
	        GetComponent<Animator>().SetTrigger("DeathTrigger");
	    }

	    if (d.magnitude < GrabRange)
	    {
            GetComponent<Animator>().SetFloat("vspeed", 0);
            GetComponent<Animator>().SetFloat("hspeed", 0);
            GetComponent<Animator>().SetTrigger("GrabTrigger");
	        _grabbing = true;
	    }
    }

    void OnGrabStart()
    {
        var d = new Vector3(_player.transform.position.x - transform.position.x, _player.transform.position.y - transform.position.y, 0);
        if (d.magnitude < GrabRange)
            _player.SendMessage("OnStun");
        else
            _grabbing = false;

    }

    void OnGrabEnd()
    {
        _player.SendMessage("OnFree");
        _grabbing = false;
        GetComponent<Animator>().SetFloat("vspeed", 0);
        GetComponent<Animator>().SetFloat("hspeed", 0);
        GetComponent<Animator>().SetTrigger("DeathTrigger");
    }

    void OnDeath()
    {
        _grabbing = false;
        _player.SendMessage("OnFree");
        Destroy(this.gameObject);
        AudioSource.PlayClipAtPoint(NurseSpawnClip, Vector3.zero);
    }
}
