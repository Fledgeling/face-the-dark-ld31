﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour
{
    public float TTL = 5f;

	void Update ()
	{
	    TTL -= Time.deltaTime;
        if(TTL < 0)
            Destroy(this.gameObject);
	}

    void DestroySelf()
    {
        Destroy(this.gameObject);
    }
}
