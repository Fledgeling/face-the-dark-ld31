﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public GameObject BulletPrefab;
    public AudioClip ShootClip;

    private const float Speed = 0.1f;
    private const float FireRate = 1.1f;

    public bool IsStunned { get { return _stunned; } }

    private float _currentRate = 0;
    private bool _stunned = false;
    
    void Update()
    {
        if (!_stunned)
        {
            var v = Input.GetAxis("VerticalMovement") * Speed;
            var h = Input.GetAxis("HorizontalMovement") * Speed;
            var movement = new Vector3(h, v, 0);
            transform.Translate(movement);
            GetComponent<Animator>().SetFloat("vspeed", -v);
            GetComponent<Animator>().SetFloat("hspeed", h);


            //rigidbody2D.MovePosition(rigidbody2D.position + new Vector2(h, v));
            transform.Translate(new Vector3(h,v,0));

            var fireVector = new Vector3(Input.GetAxis("HorizontalFire"), Input.GetAxis("VerticalFire"));
            fireVector.Normalize();
            if (fireVector.magnitude > 0)
            {
                if (_currentRate <= 0)
                {
                    var projectile = ((GameObject) Instantiate(BulletPrefab, transform.position + fireVector*1.3f, new Quaternion()));
                    projectile.GetComponent<Projectile>().Speed = 0.2f;
                    projectile.GetComponent<Projectile>().Direction = new Vector2(fireVector.x, fireVector.y);
                    projectile.GetComponent<DamageDealer>().Source = this.gameObject;
                    _currentRate = FireRate;
                    AudioSource.PlayClipAtPoint(ShootClip, Vector3.zero);
                    Debug.Log("Fire");
                }


                GetComponent<Animator>().SetFloat("vspeed", -fireVector.y);
                GetComponent<Animator>().SetFloat("hspeed", fireVector.x);
            }
            if (movement.magnitude > 0.01f)
                audio.mute = false;
            else
                audio.mute = true;
        }

        _currentRate -= Time.deltaTime;
    }

    void OnStun()
    {
        _stunned = true;
    }

    void OnFree()
    {
        _stunned = false;
    }
}
