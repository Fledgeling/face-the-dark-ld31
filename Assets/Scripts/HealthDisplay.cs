﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class HealthDisplay : MonoBehaviour
{
    public string WatchObject;
    public GameObject Heart;

    private GameObject _player;
    private int _currentHp;
    private int _maxHp;
    private GameObject[] _hearts;

	void Start ()
	{
        _player = GameObject.Find(WatchObject);
	    _player.GetComponent<Health>().OnHealthChanged += OnHeatlhChanged;

	    _currentHp = _player.GetComponent<Health>().MaxHealth;
	    _maxHp = _currentHp;
        _hearts = new GameObject[_currentHp / 2];
        for (var i = 0; i < _currentHp / 2; i++)
	    {
	        var o = (GameObject)Instantiate(Heart, Vector3.zero, new Quaternion());
	        o.transform.SetParent(this.gameObject.transform,false);
            o.transform.localScale = Vector3.one;
            o.transform.localPosition = new Vector3(i * 22, 0f, 0f);
            o.GetComponent<Animator>().SetInteger("val",2);
	        _hearts[i] = o;
	    }
	}

    void OnHeatlhChanged(Health hp, int newval)
    {
        if (newval <= 0 || newval >= _maxHp) return;
        Debug.Log("Player HEALTH: " + newval);
        var fulls = newval/2;
        var halfsAndFulls = newval/2 + newval%2;


        for (var i = 0; i < fulls; i++) _hearts[i].GetComponent<Animator>().SetInteger("val", 2);
        for (var i = fulls; i < halfsAndFulls; i++) _hearts[i].GetComponent<Animator>().SetInteger("val", 1);
        for (var i = halfsAndFulls; i < _maxHp/2; i++) _hearts[i].GetComponent<Animator>().SetInteger("val", 0);
    }
}
