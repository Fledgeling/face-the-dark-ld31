﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int MaxHealth;
    public Action<Health,int> OnHealthChanged;
    

    public int CurrentHealth
    {
        get { return _health; }
    }


    private int _health;
    
    // animation settings
    private List<SpriteRenderer> _spriteRenderers;
    private Color _baseColor;
    private int _flashes;
    private float _currentTime;
    private bool _flashed = false;
    private bool _immune = false;
    private const float _flashTime = 0.04f;
    private const int _totalFlashes = 6;

    //display settings
    public GameObject Heart;
    public string TargetUIObjectName;
    private GameObject[] _hearts;
    private GameObject _ui;
	
	void Start ()
	{
	    _health = MaxHealth;
	    if (gameObject.name.Contains("Dark"))
	        _health = 1;

	    _spriteRenderers = GetComponentsInChildren<SpriteRenderer>().ToList();
	    _baseColor = Color.white;

        if(gameObject.tag == "Player")
            InitHealthBar();
	}
	
	void Update () 
    {
	    if (_flashes > 0)
	    {
            if (_currentTime < 0)
	        {
                foreach (var spriteRenderer in _spriteRenderers)
	                spriteRenderer.color = _flashed ? _baseColor : new Color(1, 0f, 0f, 1f);    
	            _flashed = !_flashed;
	            _flashes--;
	            _currentTime = _flashTime;
	        }

            _currentTime -= Time.deltaTime;
	    }
	    else if (_flashed)
	    {
            foreach (var spriteRenderer in _spriteRenderers)
                spriteRenderer.color = _baseColor;

	        _immune = false;
	    }
	    else
            _immune = false;
    }

    public void TakeDamage(int val)
    {
        if (_immune) return;
        _health -= val;
        Debug.Log("Damage taken");
        if (OnHealthChanged != null)
            OnHealthChanged(this,_health);
        UpdateHealthBar();
        StartFlashing();
        _immune = true;
    }

    public void Restore()
    {
        _health = MaxHealth;
        if (OnHealthChanged != null)
            OnHealthChanged(this,_health);
        InitHealthBar();
        UpdateHealthBar();
    }

    public void SetHealth(int val)
    {
        _health = val;
        UpdateHealthBar();   
    }

    private void StartFlashing()
    {
        _flashes = _totalFlashes;
        _currentTime = -1;
        _flashed = false;
    }

    public void InitHealthBar()
    {
        if (_spriteRenderers == null || _spriteRenderers.Count == 0)
            Start();

        _ui = GameObject.Find(TargetUIObjectName);
        if (_ui == null) return;

        foreach (Transform child in _ui.transform)
            Destroy(child.gameObject);
        _hearts = new GameObject[_health/2];
        for (var i = 0; i < _health/2; i++)
        {
            var o = (GameObject) Instantiate(Heart, Vector3.zero, new Quaternion());
            o.transform.SetParent(_ui.transform, false);
            o.transform.localScale = Vector3.one;
            o.transform.localPosition = new Vector3(i*22, 0f, 0f);
            o.GetComponent<Animator>().SetInteger("val", 2);
            _hearts[i] = o;
        }
    }

    public void HideHealthBar()
    {
        if (_ui == null) return;
        foreach (Transform child in _ui.transform)
            Destroy(child.gameObject);
    }

    void UpdateHealthBar()
    {
        if (_ui == null || _hearts == null || _hearts.Length == 0) return;
        if (_health < 0 || _health >= MaxHealth) return;
        var fulls = _health / 2;
        var halfsAndFulls = _health / 2 + _health % 2;
        
        for (var i = 0; i < fulls; i++) _hearts[i].GetComponent<Animator>().SetInteger("val", 2);
        for (var i = fulls; i < halfsAndFulls; i++) _hearts[i].GetComponent<Animator>().SetInteger("val", 1);
        for (var i = halfsAndFulls; i < MaxHealth / 2; i++) _hearts[i].GetComponent<Animator>().SetInteger("val", 0);
    }

    public void SetImmune(bool val)
    {
        _immune = val;
    }
}
